package com.project.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class ListenerClass implements ITestListener, ISuiteListener {

    Logger logger = LoggerFactory.getLogger(ListenerClass.class);

    @Override
    public void onStart(ISuite suite) {
        logger.info("Before Suite in Listener");
    }

    @Override
    public void onFinish(ISuite suite) {
        logger.info("After Suite in Listener");
    }

    @Override
    public void onTestStart(ITestResult result) {
        logger.info("Before Method in Listener");
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        logger.info("After Method in Listener : Test Passed");
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        logger.info("After Method in Listener : Test Skipped");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        logger.info("After Method in Listener : Test Failed and here is the screenshot");
    }

}
