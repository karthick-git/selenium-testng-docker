package com.project.enums;

public enum WaitStrategy {

    CLICKABLE,
    PRESENT,
    VISIBLE,
    NONE

}
