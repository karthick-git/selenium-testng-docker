package com.project.constants;

import lombok.Getter;


public class FrameworkConstants {
    private FrameworkConstants() {
    }

    private static final String RESOURCES_PATH = System.getProperty("user.dir");
    @Getter
    private static final String PROPERTIES_PATH = RESOURCES_PATH + "/src/test/resources/application.properties";
    @Getter
    private static final String DRIVER_PATH = RESOURCES_PATH + "/src/test/resources/drivers/msedgedriver.exe";
    @Getter
    private static final String PROPERTIES_JSON_PATH = RESOURCES_PATH + "/src/test/resources/application_properties.json";
    @Getter
    private static final int EXPLICIT_WAIT = 10;

}
