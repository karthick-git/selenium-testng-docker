package com.project.pages;

import com.project.enums.WaitStrategy;
import org.openqa.selenium.By;

public class PreviewPage extends Basepage {

    private final By addToCartButton = By.xpath("//button[@type='button']");
    private final By viewCartLink = By.xpath("//u[normalize-space()='View Cart']");

    public PreviewPage addToCart() {
        click(addToCartButton, WaitStrategy.PRESENT);
        return new PreviewPage();
    }

    public CartPage viewCart() {
        click(viewCartLink, WaitStrategy.VISIBLE);
        return new CartPage();
    }
}
