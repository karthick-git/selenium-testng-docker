package com.project.pages;

import org.openqa.selenium.By;

public class CartPage extends Basepage {
    private final By itemDescriptionLink = By.xpath("//a[@href='/product_details/23']");
    private final By itemPrice = By.xpath("//td[@class='cart_price']/p");
    private final By itemQuantity = By.xpath("//td[@class='cart_quantity']/button");
    private final By totalPrice = By.xpath("//p[@class='cart_total_price']");

    public boolean verifyItemsInCart(String description, String price, String quantity, String total) {
        return matchText(itemDescriptionLink, description)
                && matchText(itemPrice, price)
                && matchText(itemQuantity, quantity)
                && matchText(totalPrice, total);
    }

}
