package com.project.pages;

import com.project.drivers.ThreadLocalDriverHelper;
import com.project.enums.WaitStrategy;
import com.project.factories.ExpilcitWaitFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Basepage {

    protected WebElement getElement(By by) {
        WebElement webElement;
        try {
            webElement = ThreadLocalDriverHelper.getDriverThreadLocal().findElement(by);
        } catch (Exception e) {
            webElement = null;
        }
        return webElement;
    }

    protected void click(By by, WaitStrategy waitStrategy) {
        ExpilcitWaitFactory.performExplicitWait(waitStrategy, by).click();
    }

    protected void sendKeys(By by, String value, WaitStrategy waitStrategy) {
        ExpilcitWaitFactory.performExplicitWait(waitStrategy, by).sendKeys(value);
    }

    protected boolean matchText(By by, String value) {
        return getElement(by).getText().equalsIgnoreCase(value);
    }

}
