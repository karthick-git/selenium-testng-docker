package com.project.pages;

import com.project.enums.WaitStrategy;
import org.openqa.selenium.By;

public final class SignInPage extends Basepage {

    private final By loginEmailTextBox = By.xpath("//input[@data-qa='login-email']");
    private final By loginPasswordTextBox = By.xpath("//input[@placeholder='Password' and @type='password']");
    private final By loginButton = By.xpath("//button[normalize-space()='Login']");


    public SignInPage enterUserName(String email) {
        sendKeys(loginEmailTextBox, email, WaitStrategy.PRESENT);
        return this;
    }

    public SignInPage enterUserPassword(String password) {
        sendKeys(loginPasswordTextBox, password, WaitStrategy.NONE);
        return this;
    }

    public HomePage clickLoginButton() {
        click(loginButton, WaitStrategy.NONE);
        return new HomePage();
    }

}
