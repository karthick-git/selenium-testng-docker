package com.project.pages;

import com.project.enums.WaitStrategy;
import org.openqa.selenium.By;

public final class HomePage extends Basepage {

    private final By signupLoginButton = By.xpath("//a[text()=' Signup / Login']");
    private final By logoutButton = By.xpath("//a[normalize-space()='Logout']");
    private final By deleteAccountButton = By.xpath("//a[normalize-space()='Delete Account']");
    private final By productsLink = By.xpath("//a[@href='/products']");

    public SignInPage clickSignupLoginButton() {
        click(signupLoginButton, WaitStrategy.PRESENT);
        return new SignInPage();
    }

    public boolean verifySuccessfulLogin() {
        return getElement(logoutButton).isDisplayed()
                && matchText(logoutButton, "Logout")
                && getElement(deleteAccountButton).isDisplayed()
                && matchText(deleteAccountButton, "Delete Account");
    }

    public SignInPage clickLogoutButton() {
        click(logoutButton, WaitStrategy.PRESENT);
        return new SignInPage();
    }

    public boolean verifySuccessfulLogout() {
        return getElement(signupLoginButton).isDisplayed()
                && matchText(signupLoginButton, "Signup / Login");
    }

    public ProductsPage clickProductsLink() {
        click(productsLink, WaitStrategy.PRESENT);
        return new ProductsPage();
    }
}
