package com.project.pages;

import com.project.enums.WaitStrategy;
import org.openqa.selenium.By;

public class ProductsPage extends Basepage {
    private final By kidsGownLink = By.xpath("//div[21]//div[1]//div[2]//ul[1]//li[1]//a[1]");

    public PreviewPage viewProduct() {
        click(kidsGownLink, WaitStrategy.PRESENT);
        return new PreviewPage();
    }


}
