package com.project.factories;

import com.project.constants.FrameworkConstants;
import com.project.drivers.ThreadLocalDriverHelper;
import com.project.enums.WaitStrategy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ExpilcitWaitFactory {

    private ExpilcitWaitFactory() {
    }

    static Duration waitTime = Duration.ofSeconds(FrameworkConstants.getEXPLICIT_WAIT());

    public static WebElement performExplicitWait(WaitStrategy waitStrategy, By by) {
        WebElement webElement = null;
        if (waitStrategy == WaitStrategy.CLICKABLE)
            webElement = new WebDriverWait(ThreadLocalDriverHelper.getDriverThreadLocal(), waitTime).until(ExpectedConditions.elementToBeClickable(by));
        else if (waitStrategy == WaitStrategy.PRESENT)
            webElement = new WebDriverWait(ThreadLocalDriverHelper.getDriverThreadLocal(), waitTime).until(ExpectedConditions.presenceOfElementLocated(by));
        else if (waitStrategy == WaitStrategy.VISIBLE)
            webElement = new WebDriverWait(ThreadLocalDriverHelper.getDriverThreadLocal(), waitTime).until(ExpectedConditions.visibilityOfElementLocated(by));
        else if (waitStrategy == WaitStrategy.NONE)
            webElement = ThreadLocalDriverHelper.getDriverThreadLocal().findElement(by);
        return webElement;
    }

}
