package com.project.helpers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.constants.FrameworkConstants;
import com.project.enums.ConfigProperties;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;

public final class JsonUtils {


    private static Map<String, Object> CONFIGMAP;

    static {
        try {
            CONFIGMAP = new ObjectMapper().readValue(new File(FrameworkConstants.getPROPERTIES_JSON_PATH()), new TypeReference<>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private JsonUtils() {

    }

    public static String getValueFromPropertiesHashMap(ConfigProperties key) {
        if (Objects.isNull(key) || Objects.isNull(CONFIGMAP.get(key.name().toLowerCase()))) {
            throw new ExceptionHelper.KeyNotFoundInPropertiesException("Property name " + key + " not found. Please check application.properties");
        }
        return (String) CONFIGMAP.get(key.name().toLowerCase());
    }
}
