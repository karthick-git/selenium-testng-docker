package com.project.helpers;

public class ExceptionHelper {
    private ExceptionHelper() {
    }

    public static class KeyNotFoundInPropertiesException extends RuntimeException {
        public KeyNotFoundInPropertiesException(String errorMessage) {
            super(errorMessage);
        }
    }

}
