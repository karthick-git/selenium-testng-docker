package com.project.helpers;

import com.project.constants.FrameworkConstants;
import com.project.enums.ConfigProperties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public final class PropertyHelper {

    private static final Properties properties = new Properties();

    private static final Map<String, String> CONFIGMAP = new HashMap<>();

    static {
        try (FileInputStream fileInputStream = new FileInputStream(FrameworkConstants.getPROPERTIES_PATH())) {
            properties.load(fileInputStream);

            /*
            //We can iterate on keySet only if keys are accessed
            for (Object key : properties.keySet()) {
                CONFIGMAP.put(String.valueOf(key), String.valueOf(properties.get(key)));
            }

            //We can iterate on entrySet, if both keys and values are accessed
            for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                CONFIGMAP.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
            }*/

            //This is the advanced implementation of the above loop
            properties.forEach((key, value) -> CONFIGMAP.put(String.valueOf(key), String.valueOf(value).trim()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private PropertyHelper() {

    }

    /*public static String getValueFromPropertiesHashTable(String key) {
        if (Objects.isNull(key) || Objects.isNull(CONFIGMAP.get(key))) {
            throw new ExceptionHelper.KeyNotFoundInPropertiesException("Property name " + key + " not found. Please check application.properties");
        }
        return CONFIGMAP.get(key);
    }*/

    public static String getValueFromPropertiesHashMap(ConfigProperties key) {
        if (Objects.isNull(key) || Objects.isNull(CONFIGMAP.get(key.name().toLowerCase()))) {
            throw new ExceptionHelper.KeyNotFoundInPropertiesException("Property name " + key + " not found. Please check application.properties");
        }
        return CONFIGMAP.get(key.name().toLowerCase());
    }
}
