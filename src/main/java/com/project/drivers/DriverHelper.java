package com.project.drivers;

import com.project.constants.FrameworkConstants;
import com.project.enums.ConfigProperties;
import com.project.helpers.JsonUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

import java.util.Objects;

public class DriverHelper {

    private DriverHelper() {
    }

    public static void initializeDriver() {
        if (Objects.isNull(ThreadLocalDriverHelper.getDriverThreadLocal())) {
//            WebDriverManager.edgedriver().setup();
            System.setProperty("webdriver.edge.driver", FrameworkConstants.getDRIVER_PATH());
            WebDriver driver = new EdgeDriver();
            ThreadLocalDriverHelper.setDriverThreadLocal(driver);
            ThreadLocalDriverHelper.getDriverThreadLocal().get(JsonUtils.getValueFromPropertiesHashMap(ConfigProperties.URL));
        }
    }

    public static void quitDriver() {
        if (Objects.nonNull(ThreadLocalDriverHelper.getDriverThreadLocal())) {
            ThreadLocalDriverHelper.getDriverThreadLocal().quit();
            ThreadLocalDriverHelper.unloadDriverThreadLocal();
        }
    }
}
