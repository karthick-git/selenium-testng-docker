package com.project.drivers;

import org.openqa.selenium.WebDriver;

public final class ThreadLocalDriverHelper extends ThreadLocal<String> {

    private ThreadLocalDriverHelper() {
    }

    //Normal initialization - when get() invoked before initialization results in null
    private static final ThreadLocal<WebDriver> driverThreadLocal = new ThreadLocal<>();

    //Advanced initialization with Anonymous class - when get() invoked before initialization results the default driver instance
//    private static final ThreadLocal<WebDriver> driverThreadLocal = new ThreadLocal<>(){
//        @Override
//        protected WebDriver initialValue() {
//            WebDriverManager.edgedriver().setup();
//            return new EdgeDriver();
//        }
//    };

    //Advanced initialization with Supplier Interface - when get() invoked before initialization results the default driver instance
//    private static final ThreadLocal<WebDriver> driverThreadLocal = ThreadLocal.withInitial(() -> {
//        WebDriverManager.edgedriver().setup();
//        return new EdgeDriver();
//    });

    public static WebDriver getDriverThreadLocal() {
        return driverThreadLocal.get();
    }

    public static void setDriverThreadLocal(WebDriver driverReference) {
        driverThreadLocal.set(driverReference);
    }

    public static void unloadDriverThreadLocal() {
        driverThreadLocal.remove();
    }


}
