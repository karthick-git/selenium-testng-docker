package com.suite.tests;

import com.project.drivers.DriverHelper;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {

    protected BaseTest() {
    }

    @BeforeMethod
    protected void setUp() {
        DriverHelper.initializeDriver();
    }

    @AfterMethod
    protected void tearDown() {
        DriverHelper.quitDriver();
    }


}
