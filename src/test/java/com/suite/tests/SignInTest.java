package com.suite.tests;

import com.project.pages.CartPage;
import com.project.pages.HomePage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SignInTest extends BaseTest {
    private SignInTest() {
    }

    @Test(dataProvider = "loginTestDataProvider")
    public void signInSignOutTest(String username, String password) {
        HomePage homePage = new HomePage().clickSignupLoginButton()
                .enterUserName(username)
                .enterUserPassword(password)
                .clickLoginButton();

        assertThat(homePage.verifySuccessfulLogin()).as("Login unsuccessful").isTrue();
        homePage.clickLogoutButton();
        assertThat(homePage.verifySuccessfulLogout()).as("Logout unsuccessful").isTrue();
    }

    @Test(dataProvider = "addProductDataProvider")
    public void addProductToCartTest(String description, String price, String quantity, String total) {
        CartPage cartPage = new HomePage().clickProductsLink()
                .viewProduct()
                .addToCart().viewCart();

        assertThat(cartPage.verifyItemsInCart(description, price, quantity, total)).as("Login unsuccessful").isTrue();

    }

    @DataProvider(name = "loginTestDataProvider", parallel = true)
    public Object[][] getUserData() {
        return new Object[][]{
//                {"karthickash007@gmail.com", "12345"},
                {"karthickash007@gmail.com", "123456"}
        };
    }

    @DataProvider(name = "addProductDataProvider", parallel = true)
    public Object[][] getProductData() {
        return new Object[][]{
//                {"karthickash007@gmail.com", "12345"},
                {"Sleeveless Unicorn Print Fit & Flare Net Dress - Multi", "Rs. 1100", "1", "Rs. 1100"}
        };
    }

}
