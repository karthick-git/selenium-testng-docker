# Tools and Tech Stack

- Java
- Selenium
- WebDriverManager
- TestNG
- Maven
- AssertJ
- PageObjectModel, Singleton, and Factory Design patterns
- PageFactory, By, String
- ThreadLocal - ThreadSafety
- ExtentReports -5.0.5
- Excel sheet for test data maintenance(data provider)
- Property or JSON as a config file.
- Use TestNG listeners like Annotation Transformer, ITestListener, ISuiteListener
- Running tests in the cloud or dockerized selenium grid
- Parallel cross-browser testing
- Log4j and SLF4J

# Advanced features

- Real-time dashboard with ELK images.
- Using SonarLint to write clean code
- Java 8 - Method references, Lambdas, Streams to optimize our code Selenoid integration

# Rules to be followed while creating a framework

- Never hardcode.
- Keep the right things in the right place.
- Religiously practice the Reuse culture.
- Make the framework easy for manual testers or others to contribute.
- Make the framework robust