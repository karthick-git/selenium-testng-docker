### Best practices

#### Driver Initialization

- BaseTest should call DriverHelper methods for driver initialization
- Always check if the driver is null before creating its instance to avoid multiple driver instance initializations
- Use Objects class for null checks instead of directly checking like objName== null
- If a class shouldn't be extended, make the class final
- If an object shouldn't be created for a class, add a private constructor to the class
- If a child class is extending a parent class, then add a protected constructor instead of private or public
- Instead of hard coding the base path, use fetch the "user. dir" from the code

#### MultiThreading

- In Java, for the local variables of primitive types, both the variable and value are stored in the stack while for the
  local variables of non-primitive types, the variables (only the corresponding hashcode) are stored in the stack but
  the objects are stored in the heap
- An object's member(instance) variable, both primitive and non-primitive, will be stored in heap along with the object
  itself. So if a thread has access to an object, it also has access to its variables and methods.
- The static class variables are stored in the heap. So always use ThreadLocal if test cases are going to be run in
  parallel. ThreadLocal class helps to create variables that can be accessed using just one thread at a time
- Do null checks for keys while reading values from the property file
- Use ThreadLocalDriverHelper.getDriverThreadLocal() to refer to the driver throughout the project, don't assign it to
  any reference variable
- Use static blocks for eager initialization, since it will be loaded into memory along with class files (Note: Use this
  only if you think the variable will be used while running tests, otherwise opt for lazy initialization)
- Multiple static blocks can be used and the last static block will take precedence (This is not recommended)

#### Property file usage

- Property file once loaded into memory behaves like a hashtable (it's a little slow but thread-safe)
- If the property file is a load once, use once, leave it as a hashtable but if load once read many times, converting
  the hashtable to a hashmap
- Trim the spaces in the end while fetching values from the property file

#### Page Object Model + Encapsulation

- Use page object design pattern when doing UI automation. It is a structural design pattern that helps us move the code
  that is not related to the tests (locators and methods) to separate classes. We can create each class for each page in
  a web application.
    - Advantages
        - Effective segregation
        - Functional encapsulation
        - Scalable
    - Disadvantages
        - High setup time
        - Ambiguity in maintenance

#### Method chaining

- To do method chaining with the same class, return *this* in a method with the return type as the same class name and
  To do method chaining with a different class, return *new `< Other class names>`* in a method with the return type as
  the other class' name

#### Page Factory

- Page Factory is a design pattern that may or may not be coupled with the Page object model.
    - Some myths about page factory
        - Reduces verbosity
        - Faster than using findElement
        - Lazy evaluation
    - Problems with Page Factory
        - StaleElementReferenceException
        - NullPointerException
        - Cannot be used to produce dynamic elements

#### Do's and Don't

- Don't add any assertions in the page classes, instead return the element and then perform assertions in the test
  classes
- Thread. sleep is not recommended, but in case it has to be used, there is a method from apache commons lang3 library's
  Uninterruptibles class called sleepUninterruptibly which will perform the same task but without asking us to handle
  exceptions
- Name the variables to indicate what it is actually. With a prefix of suffix with _link, _dropdown, _button, etc. e.g -
  logoutButton or logout_button or logout_btn
- All page methods need to have some return type

#### Inheritance

- Don't use inheritance for code re-usability alone. Only use it if a subclass IS A superclass. So use it only when the
  subclass needs most of or all of the methods of the superclass In simple words, the test classes should extend the
  base test class and the pages should extend the base page class.

#### Abstraction

- Instead of directly performing operations on the variables, use the corresponding get methods. This gives us more
  power in terms of doing conditional checks

#### Enum

- If we want to make sure that the user uses only a predefined set of values, we have to use enumerations or enums
    - Enum is a collection of constants and methods etc.
    - It can be used with if, switch, etc. and also with instance variables
    - It cannot extend another class
    - We can make use of the methods like value(), valueOf(), toString()
    - We can store enums in enumSet or enumMap
    - For enums, the java compiler by default adds a toString()
- Sample use cases of enums are webDriver wait strategy and property file

#### DataProvider

- Why use DataProvider instead of for loop to iterate? If for loop is used, the test count is always 1, even if there
  are 100 iterations performed. If the 1st iteration fails, we won't know the remaining iterations won't be executed.
  DataProvider treats each iteration as a different test case.
- If we haven't specifically given a name to the dataProvider, the method name will be the name of the dataProvider. For
  e.g - @Test(dataProvider="getData")
- A common misconception is that dataProvider should return a 2-D Object array, but that isn't the case, we can return
  anything with dataProvider
- In a 2D- Object array e.g - employeeArray[2][3] - 2 is the number of iterations that the test has to be run and 3 is
  the number of parameters that we want to feed our test
- Add parallel=true to the dataProvider method to run the test cases in parallel, in this case the thread count
  mentioned in the testNg XML won't be taken into consideration
- To control the number of threads that dataProvider can run in parallel, mention data-provider-thread-count="X" near
  the suite name in TestNg XML

#### Listeners

- ITestListener ISuiteListener, IMethodInterceptor, IRetryListener, IAnnotationTransformer are provided by TestNG
- We have to override these methods in our Listener Class that will extend the ITestListener & ISuiteListener.
- Providing our owm implementation to these methods is recommended. If a method is not required, we can simply skip it
  with no implementation
- onStart and onFinish methods in ISuiteListener are similar to BeforeSuite and AfterSuite Methods
- onTestStart, onTestSuccess, onTestFailure, onTestSkipped. onTestFailedButWithinSuccessPercentage are similar to
  BeforeTest and AfterTest methods
- To add a listener to the framework, add a listener tag to the TestNg XML and mention the path of the listener class
- Order of execution
    - Before suite in Listener
    - Before suite in Runner
    - Before method in Runner
    - Before method in Listener
    - Test method
    - After method in Listener
    - After method in Runner
    - After suite in Runner
    - After suite in Listener
- To implement method interceptors, create a class that implements IMethodInterceptor and override the only method in
  that interface -> intercept()
- The intercept method contains the list of all tests that are going to be executed by TestNg
- If we want to run only certain tests, we can manually create a list of type IMethodInstance and add the tests that
  needed to be run and return that in the intercept method
- To add an interceptor to the framework, add a listener tag to the TestNg XML and mention the path of the intercept
  class
- Multiple listeners can be added in a framework
- The onStart method can be used for establishing DB connection, initializing reporters etc.

